import inspect
from functools import wraps
from typing import Callable, Dict, Iterable

from os_env_injection._core import inject_var
from os_env_injection._decorator_directives import Injection


def inject_os_env(injections: Iterable[Injection] = ()) -> Callable:
    # pylint: disable=invalid-name
    def inject(f: Callable) -> Callable:
        @wraps(f)
        def inject_f(*args, **kwargs):
            kwargs = _turn_args_into_kwargs(args, kwargs)
            kwargs = _integrate_with_os_env(kwargs=kwargs)
            return f(**kwargs)

        def _turn_args_into_kwargs(args: tuple, kwargs: Dict[str, object]) -> Dict[str, object]:
            kwargs_complement = {k: args[i] for i, k in enumerate(inspect.getfullargspec(f).args) if i < len(args)}
            kwargs.update(kwargs_complement)
            return kwargs

        def _integrate_with_os_env(kwargs: dict) -> Dict[str, object]:
            kw_defaults = _extract_f_defaults()
            # pylint: disable=invalid-name
            for v in injections:
                kwargs[v.var_name] = inject_var(
                    os_env_key=v.os_env_key,
                    var_value=kwargs.get(v.var_name, kw_defaults.get(v.var_name, None)),
                    is_required=v.required,
                )
            return kwargs

        def _extract_f_defaults() -> Dict[str, object]:
            defaults = inspect.getfullargspec(f).defaults

            if defaults is None:
                return {}

            args = inspect.getfullargspec(f).args
            return dict(zip(args[::-1], defaults[::-1]))

        return inject_f

    return inject
